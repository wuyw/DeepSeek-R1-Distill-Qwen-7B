import torch
import torch_npu
import gradio as gr
from threading import Thread
from modelscope import snapshot_download
from transformers import AutoModelForCausalLM, AutoTokenizer, TextIteratorStreamer


MODEL_ID = "deepseek-ai/DeepSeek-R1-Distill-Qwen-7B"
MODEL_NAME = MODEL_ID.split("/")[-1]
CONTEXT_LENGTH = 2000
DESCRIPTION = f"当前仅提供 {MODEL_NAME} 模型部署实例，有算力的可自行克隆至本地进行测试"
MODEL_DIR = "model/DeepSeek-R1-Distill-Qwen-7B"


def predict(
    message,
    history,
    system_prompt,
    temperature,
    max_new_tokens,
    top_k,
    repetition_penalty,
    top_p,
):
    # Format history with a given chat template
    stop_tokens = ["<|endoftext|>", "<|im_end|>", "|im_end|"]
    instruction = "<|im_start|>system\n" + system_prompt + "\n<|im_end|>\n"
    for user, assistant in history:
        instruction += f"<|im_start|>user\n{user}\n<|im_end|>\n<|im_start|>assistant\n{assistant}\n<|im_end|>\n"

    instruction += f"<|im_start|>user\n{message}\n<|im_end|>\n<|im_start|>assistant\n"
    try:
        streamer = TextIteratorStreamer(
            tokenizer,
            skip_prompt=True,
            skip_special_tokens=True,
        )
        enc = tokenizer(instruction, return_tensors="pt", padding=True, truncation=True)
        input_ids, attention_mask = enc.input_ids, enc.attention_mask
        if input_ids.shape[1] > CONTEXT_LENGTH:
            input_ids = input_ids[:, -CONTEXT_LENGTH:]
            attention_mask = attention_mask[:, -CONTEXT_LENGTH:]

        generate_kwargs = dict(
            input_ids=input_ids.to(device),
            attention_mask=attention_mask.to(device),
            streamer=streamer,
            do_sample=True,
            temperature=temperature,
            max_new_tokens=max_new_tokens,
            top_k=top_k,
            repetition_penalty=repetition_penalty,
            top_p=top_p,
        )
        t = Thread(target=model.generate, kwargs=generate_kwargs)
        t.start()

    except Exception as e:
        streamer = f"{e}"

    outputs = []
    for new_token in streamer:
        outputs.append(new_token)
        if new_token in stop_tokens:
            break

        yield "".join(outputs)


if __name__ == "__main__":
    device = torch.device("npu:0" if torch.npu.is_available() else "cpu")
    tokenizer = AutoTokenizer.from_pretrained(MODEL_DIR)
    model = AutoModelForCausalLM.from_pretrained(MODEL_DIR, device_map="auto")
    # Create Gradio interface
    gr.ChatInterface(
        predict,
        title=f"{MODEL_NAME} 部署实例",
        description=DESCRIPTION,
        additional_inputs_accordion=gr.Accordion(label="⚙️ 参数设置", open=False),
        additional_inputs=[
            gr.Textbox(
                "You are a useful assistant. first recognize user request and then reply carfuly and thinking",
                label="系统提示词",
            ),
            gr.Slider(0, 1, 0.6, label="温度参数"),
            gr.Slider(0, 32000, 10000, label="最大 token 数"),
            gr.Slider(1, 80, 40, label="Top-K 采样"),
            gr.Slider(0, 2, 1.1, label="重复性惩罚"),
            gr.Slider(0, 1, 0.95, label="Top-P 采样"),
        ],
    ).queue().launch()